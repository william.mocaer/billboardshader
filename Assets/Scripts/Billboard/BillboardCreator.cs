﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BillboardCreator : MonoBehaviour
{
    public GameObject prefabMeshBillboard;
    public List<Transform> points;

    // Start is called before the first frame update
    private void DrawMeshContainingBillboard(List<Vector3> points)
    {
        //considering that it's in the right order
        GameObject mesh = Instantiate(prefabMeshBillboard);
        mesh.name = "meshBillboards";
        mesh.transform.position = Vector3.zero;
        MeshFilter mf = mesh.GetComponent<MeshFilter>();
        Mesh m = new Mesh();
        m.name = "billboards";
        m.vertices = points.ToArray();
        m.SetIndices(Enumerable.Range(0, points.Count).ToArray(),MeshTopology.Points,0);
        m.Optimize();
        m.RecalculateBounds();
        mf.mesh = m;
        
    }


    private void Start()
    {
        DrawMeshContainingBillboard(points.Select(t=>t.position).ToList());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
