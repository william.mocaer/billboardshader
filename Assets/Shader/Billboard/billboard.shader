﻿Shader "Custom/billboard"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
       Tags {
                "RenderType" = "Transparent"
				"Queue" = "Transparent" 
        }
        
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma geometry geom

            #include "UnityCG.cginc"
            #define PI 3.14159265

            struct appdata{
                float4 vertex : POSITION;
            };
             
            struct v2g{
                float4 pos : SV_POSITION;
                float4 poslocal : TEXCOORD0;
            };
             
            struct g2f{
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            
            v2g vert (appdata v)
            {
                v2g o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.poslocal = (v.vertex);
                return o;
            }
            
            [maxvertexcount(6)]
            void geom (point v2g input[1], inout TriangleStream<g2f> tristream){
            
                float3 positionPoint = input[0].pos.xyz/input[0].pos.w;
    
    
                float3 x = float3(1,0,0)*(_MainTex_TexelSize.z*(1/_ScreenParams.x));//1/size
                float3 up = float3(0,1,0)*(_MainTex_TexelSize.w*(1/_ScreenParams.y));
                
                /*float3 P0 = float3(0,0,positionPoint.z);//positionPoint - x - up ;
                 float3 P1 = float3(1,-0.5,positionPoint.z);//positionPoint - x + up;
                 float3 P2 = float3(0,0.4,positionPoint.z);//positionPoint + x - up ;
                float3 P3 = float3(0.9,0.9,positionPoint.z);//positionPoint + x + up;
                */
                 float3 P0 = positionPoint - x - up ;
                 float3 P1 = positionPoint + x - up;
                 float3 P2 = positionPoint - x + up ;
                float3 P3 = positionPoint + x + up;
                //face verticale1
             
                g2f iP0;
                iP0.pos = (float4(P0,1));
                iP0.uv = float2(0,1);
                
                g2f iP1;
                iP1.pos = (float4(P1,1));
                iP1.uv = float2(1,1);
                
                g2f iP2;
                iP2.pos = (float4(P2,1));
                iP2.uv = float2(0,0);
                
                g2f iP3;
                iP3.pos = (float4(P3,1));
                iP3.uv = float2(1,0);
                
                tristream.Append(iP3);
                tristream.Append(iP2);
                tristream.Append(iP0);

                tristream.RestartStrip();
                //--          
                tristream.Append(iP3);
                tristream.Append(iP0);  
                tristream.Append(iP1);

                tristream.RestartStrip();
              }
            fixed4 frag (g2f i) : SV_Target
            {
                float4 col =  tex2D(_MainTex,i.uv);
                return col;
            }
            ENDCG
        }
    }
}
